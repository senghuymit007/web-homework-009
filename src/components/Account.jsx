import React, { Component } from "react";
import { Link, BrowserRouter as Router, Switch, Route, useParams,  useRouteMatch, } from "react-router-dom";

export default function Account(){
    let { path, url } = useRouteMatch();
    return (
        
      <div>
        <Router>
          <h2>Account</h2>
          <ul>
            <li>
              <Link to={`${url}/Netflix`}>Netflix</Link>
            </li>
            <li>
              <Link to={`${url}/Zilow Group`}>Zilow Group</Link>
            </li>
            <li>
              <Link to={`${url}/Yahoo`}>Yahoo</Link>
            </li>
            <li>
              <Link to={`${url}/Mudus Create`}>Mudus Create</Link>
            </li>
          </ul>

          <Switch>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
        </Router>
      </div>
    );
}

function Topic() {
    let { topicId } = useParams();
  
    return (
      <>
        <h4>
         ID: <span style={{ color: "red" }}>{topicId}</span>{" "}
        </h4>
      </>
    );
  }

  
  
