import React from "react";
import { Card, Button, Col, Container, Row } from "react-bootstrap";
import {  Link } from "react-router-dom";

export default function Home({ anime }) {
    
  console.log("Props on func: ", anime);

  let temp = anime.filter((item) => {
    return item.id > 0;
  });

  //const historyDetail = useHistory();

//   function gotoDetail() {
//     historyDetail.push("/homedata")
//   }

  return (
    <>
      {temp.map((item, index) => (
            <Col xs="3" key={index}>
              <Card style={{ width: "15rem" }}>
                <Card.Img variant="top" width="100%" height="150px" src={item.thumbnail} />
                <Card.Body>
                  <Card.Title>{item.title}</Card.Title>
                  <Card.Text>{item.description}</Card.Text>
                  <Link to={`/homedata/${item.id}`}>
                      <Button variant="secondary" size="sm">Read</Button>
                    </Link>
                </Card.Body>
              </Card><br />
            </Col>
      ))}
    </>
  );
}
